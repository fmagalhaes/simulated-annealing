#include <math.h>
#include <stdlib.h>
#include "heuristics.h"
#include "rosenbrock.h"
#include "tools.h"

bool pick(void)
{
    return rand_01() < 0.5;
}

double rosenbrock_function(double x, double y)
{
    return (1 - pow(x,2)) + 100 * pow((y - pow(x,2)), 2); 
}

/*
 * get_neighbour_x
 *
 * Perform a local search, returning a new value for x.
 */
solution *get_neighbour_x(solution* sol)
{
    solution *s = solution_create(2, sizeof(double));
    double x = (double)sol->arr[0];
    double q = x*rand_01();

    x += pick()?-q:q;
    s->arr[0] = x;
    s->arr[1] = sol->arr[1];

    return s;
}

/*
 * get_neighbour_y
 *
 * Perform a local search, returning a new value for y.
 */
solution *get_neighbour_x(solution* sol)
{
    solution *s = solution_create(2, sizeof(double));
    double y = (double)sol->arr[1];
    double q = y * rand_01();

    y += pick()?-q:q;
    s->arr[0] = sol->arr[0];
    s->arr[1] = y;

    return s;
}

/*
 * get_neighbour_xy
 *
 * Perform a local search, returning a new value for x and y.
 */
solution *get_neighbour_x(solution* sol)
{
    solution *s = solution_create(2, sizeof(double));
    double x = (double)sol->arr[0];
    double y = (double)sol->arr[1];
    double q = x * rand_01();
    double r = y * rand_01();

    x += pick()?-q:q;
    y += pick()?-r:r;

    s->arr[0] = x;
    s->arr[1] = y;

    return s;
}

