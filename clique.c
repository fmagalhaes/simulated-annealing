#include "clique.h"

typedef struct {
    int v;      /* Vertex number. */
    int d;      /* Vertex degree. */
} item;

static int compare_degrees(const void *vertex1, const void *vertex2);
static void copy_vertices(graph *g, item *arr);
static int *item_to_int_array(item *arr, size_t nitems);
static size_t f(solution *sol, graph *g);
static int rand_2();
static void rollback_neighbour(solution *sol, int old_v, int new_v);
static void get_neighbour(solution *sol, int *old_v, int *new_v);
static size_t get_neighbour_n(solution *sol, graph *g, int *old_v, int *new_v, bool *gave_up);
static size_t get_neighbour_backtrack(solution *sol, graph *g, int *old_v, int *new_v, bool *gave_up);

/*
 * compare_degrees
 *
 * Compare the degrees of two vertices. Used to sort vertices by descending order of degree.
 */
static int compare_degrees(const void *vertex1, const void *vertex2)
{
    int v1 = ((item*)vertex1)->d;
    int v2 = ((item*)vertex2)->d;

    return (v1>v2)?
                -1
            :
                (v1<v2)?
                    1
                :
                    0;
}

/*
 * copy_vertices
 *
 * Copy graph vertices to the array of items. It's your responsability to allocate an array of
 * correct size.
 */
static void copy_vertices(graph *g, item *arr)
{
    int i;
    for (i = 0; i < g->nvertices; i++) {
        arr[i].v = i;
        arr[i].d = g->degrees[i];
    }
}

/*
 * item_to_int_array
 *
 * Create an array of int representing vertices, in order, copied from arr.
 */
static int *item_to_int_array(item *arr, size_t nitems)
{
    size_t i;
    int *a = (int *)emalloc(sizeof(int) * nitems);

    for (i = 0; i < nitems; i++) {
        a[i] = arr[i].v;
    }

    return a;
}

/*
 * f
 *
 * Calculate the objective of solution, i.e. the clique size specified by it.
 */
static size_t f(solution *sol, graph *g)
{
    size_t M = 0;
    bool is_broken = false;
    int i,j;

    for (i = 0; i < sol->arr_length; i++) {

        for (j = 0; j < i; j++) {
            if (g->adj_matrix[sol->arr[i]][sol->arr[j]] != 1) {
                is_broken = true;
                break;
            }
        }

        if (is_broken)
            break;

        M++;

    }

    return M;
}

/*
 * test_f
 *
 * Test f function. For this, define a graph of 6 vertices containing a clique of 5 vertices and
 * test if f returns appropriate objective value.
 */
bool test_f(void)
{
    graph *g = graph_create(6);
    solution *sol = solution_create(6);
    size_t obj;
    bool res = true;

    EDGE(g, 0, 1);
    EDGE(g, 0, 2);
    EDGE(g, 0, 3);
    EDGE(g, 0, 4);
    EDGE(g, 1, 2);
    EDGE(g, 1, 3);
    EDGE(g, 1, 4);
    EDGE(g, 2, 0);
    EDGE(g, 2, 3);
    EDGE(g, 2, 4);
    EDGE(g, 3, 0);
    EDGE(g, 3, 1);
    EDGE(g, 3, 4);
    /*
 *      * Create a few edges with node 5 but not enough to integrate it into a clique.
 *           */
    EDGE(g, 0, 5);
    EDGE(g, 1, 5);
    EDGE(g, 2, 5);

    sol->arr[0] = 0;
    sol->arr[1] = 1;
    sol->arr[2] = 2;
    sol->arr[3] = 3;
    sol->arr[4] = 4;
    sol->arr[5] = 5;
    
   obj = f(sol, g);

    if (obj != 5)
        res = false;

    //printf("Obtained a f of %zu\n", obj);
    solution_free(sol);
    graph_free(g);
    return res;
}

/*
 * gen_init_sol
 *   *
 *    * Generate an initial solution. Our approach is to sort the vertices from the larger degree to the least and then
 *     * try to build the largest solution possible.
 *      *
 *       * Return:
 *        *      - A solution. Don't forget to free the memory.
 */
solution *gen_init_sol(graph *g)
{
    /* Current clique size. */
    size_t M = 0;
    size_t i = 0;
    bool break_clique = false;
    item *arr = (item *)emalloc(sizeof(item) * g->nvertices);
    solution *sol = (solution *)emalloc(sizeof(solution));

    copy_vertices(g, arr);
    /* Create an array of items and sort it by highest degree. */
    qsort((void *)arr, (size_t)g->nvertices, sizeof(item), compare_degrees);

    /*
     *  Iterate through the array and verify if we have a clique. For each item in array, verify if we have edges
     *  connecting this new vertex and all other vertices. If so, consider it a part of sollution and increment M,
     *  the size of our click. Do this until finding a vertex that won't form a clique. Further improvement of this
     *  solution is responsability of SA heuristic.
     */
    while(true) {
        for(i = 0; i < M; i++) {
            if (g->adj_matrix[arr[i].v][arr[M].v] != 1) {
                break_clique = true;
                break;
            }
        }

        if (break_clique) {
            break;
        }
        M++;
    }  
    sol->arr = item_to_int_array(arr, g->nvertices);
    sol->arr_length = g->nvertices;
    sol->sol_length = M;
    return sol;
} 


/*
 *  * get_neighbour
 *   *
 *    * Choose ramdomly a vertex from clique and one outside clique, swaping both. If solution size is zero, add first vertex.
 *     */
void get_neighbour(solution *sol, int *old_v, int *new_v)
{
    if (sol->sol_length != 0) {
        int range_out = sol->arr_length - sol->sol_length;
        int exiting = rand() % sol->sol_length;
        int entering = sol->sol_length + (rand() % range_out);

        *old_v = exiting;
        *new_v = entering;

        sol->arr[*old_v] ^= sol->arr[*new_v];
        sol->arr[*new_v] ^= sol->arr[*old_v];
        sol->arr[*old_v] ^= sol->arr[*new_v];
    } else {
        sol->sol_length++;
    }
}

/*
 * get_neighbour_n
 *
 * Perform a local search returning a neighbour solution of sol. To achieve this we try to add a random vertex to the solution
 * creating a new clique one item bigger than the given solution. If haven't found a vertex in 128 tries, give up.
 *
 * Return:
 *  The objective of the found clique (i.e. its size).
 */
size_t get_neighbour_n(solution *sol, graph *g, int *old_v, int *new_v, bool *gave_up)
{
    int range_out = sol->arr_length - sol->sol_length;
    int leaving = sol->sol_length;
    int entering = sol->sol_length + (rand() % range_out);
    int n = 128;                         /* Maximum number of retries for a feasible solution. */
    int best_f = 0;

    *gave_up = false;

    if (sol->sol_length != 0) {
            /*
             * Do a local search adding a vertex.
             */
            ARRAY_SWAP(sol->arr, entering, leaving);

            /*
             * Try until we find a feasible solution. Give up if can't find a feasible solution in reasonable time.
             */
            while ((best_f = f(sol, g)) <= sol->sol_length) {
                if (n == 0) {
                    *gave_up = true;
                    break;
                }

                //ARRAY_SWAP(sol->arr, leaving, entering);
                entering = sol->sol_length + (rand() % range_out);
                ARRAY_SWAP(sol->arr, entering, leaving);
                n--;
            }

            sol->sol_length = best_f;
    } else {
        sol->sol_length++;
    }

    //if (*gave_up) {
    //    sol->sol_length--;
    //}

    *old_v = leaving;
    *new_v = entering;

    return sol->sol_length;
}

/*
 * tracked_random
 *
 * Yield a random node number not present in black-list.
 */
size_t tracked_random(size_t min, size_t max, hash_node *blacklist[])
{
    /* If hash table has a occupancy of 80%, return error. */
    static int max_occup = NHASH * 0.8;
    int rnd = min + (rand() % max);

    while (hash_lookup(blacklist, rnd, 0) != NULL) {
        rnd = min + (rand() % max);
    }

    hash_lookup(blacklist, rnd, 1);
    --max_occup;

    return (max_occup>0)?rnd:SIZE_MAX;
}

static hash_node *node_table[NHASH];

#define VALID_RND(x) (x!=SIZE_MAX)

static size_t get_neighbour_backtrack(solution *sol, graph *g, int *old_v, int *new_v, bool *gave_up)
{
    int range_out = sol->arr_length - sol->sol_length;
    int leaving = sol->sol_length;
    int entering; // = sol->sol_length + (rand() % range_out);
    //int n = g->nvertices/(int)log(g->nvertices);                         /* Maximum number of retries for a feasible solution. */
    int n = 5;
    int best_f = 0;
    int backtrack_level = 512;

    entering = tracked_random(sol->sol_length, range_out, node_table);
    *gave_up = false;

    if (sol->sol_length != 0) {
            /*
             * Do a local search adding a vertex.
             */
            ARRAY_SWAP(sol->arr, entering, leaving);

            /*
             * Try until we find a feasible solution. Give up if can't find a feasible solution in reasonable time.
             */
            while ((best_f = f(sol, g)) <= sol->sol_length) {
                /*
                 * If we exhausted retries we most likely have crappy vertices. Remove them and start searching all over
                 * again. Track down removed vertices to avoid adding them again.
                 */
                if (n == 0) {
                    //printf("Backtracking! Node: %d\n", sol->arr[sol->sol_length-1]);
                    hash_lookup(node_table, sol->arr[sol->sol_length-1], 1);
                    sol->sol_length--;
                    n = 2;

                    if (backtrack_level == 0) {
                        gave_up = true;
                        break;
                    }

                    backtrack_level--;
                }
                n--;

                ARRAY_SWAP(sol->arr, leaving, entering);
 /*
                 * Avoid blacklisted vertices.
                 */
                entering = tracked_random(sol->sol_length, range_out, node_table);

                /*
                 * If more than 80% of vertices are blacklisted, give up.
                 */
                if (!VALID_RND(entering)){
                    gave_up = true;
                    break;
                }

                ARRAY_SWAP(sol->arr, entering, leaving);
            }

            sol->sol_length = best_f;
            //printf("Added vertex: %d\n", entering);
    } else {
        sol->sol_length++;
    }

    if (gave_up) {
        sol->sol_length--;
    }

    *old_v = leaving;
    *new_v = entering;

    return sol->sol_length;
}

