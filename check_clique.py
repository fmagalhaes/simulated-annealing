from collections import defaultdict
from sys import exit

clique = raw_input("Clique: ")

edges = defaultdict(list)

with open('instances/brock800_1.clq') as f:
    lines = f.readlines()

for l in lines:
    e = l.split()
    if e[0] == 'e':
        edges[int(e[1])].append(int(e[2]))

clique = clique.split()

print len(clique)

for i in range(0, len(clique)):
    for j in range(0, len(clique)):
        if i != j:
            m = max(int(clique[i]), int(clique[j]))
            l = min(int(clique[i]), int(clique[j]))

            #print 'Max: %d Min: %d' %(m, l) 

            if l not in edges[m]:
                print 'Not a clique: %d-%d' % (m,l)
                exit()
                 
