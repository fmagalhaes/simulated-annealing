#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "datatypes.h"
#include "graph.h"
#include "tools.h"

#define MAXLINELEN      256              /* Maximum file line length */ 
#define FILEDELIM      "\t "             /* Valid delimiters for fields in a file */

/*
graph *graph_copy(graph *g)
{
    graph *new_g = malloc(sizeof(graph));
    new_g->edges = malloc(sizeof(vertex) * g->nvertices);
    memcpy(new_g->edges, g->edges, sizeof(vertex) * g->nvertices);
}

graph *graph_create(int nvertices)
{
    graph *g;
    int i;
    
    g = (graph*)malloc(sizeof(graph));
    g->edges = (vertex*)malloc(nvertices * sizeof(vertex));
    g->degrees = (int*)malloc(nvertices * sizeof(int));
    g->nvertices = nvertices;
    return g;
}

void graph_free(graph *g)
{
    int i;

    for (i = 0; i < g->nvertices; i++) {
        list_free(g->edges[i]->next);
    }
    free(g->degrees);
    free(g);
}
*/

graph* graph_create(int nvertices)
{
    graph *g;
    int i;

    g = emalloc(sizeof(graph));
    g->adj_matrix = (char **)emalloc(sizeof(char*) * nvertices);
    for (i = 0; i < nvertices; i++) {
        g->adj_matrix[i] = (char *)emalloc(sizeof(char) * nvertices);
    }
    g->degrees = (int *)emalloc(sizeof(int) * nvertices);

    return g;
}

void graph_free(graph *g)
{
    int i;

    for (i = 0; i < g->nvertices; i++) {
        free(g->adj_matrix[i]);
    }
    
    free(g->degrees);
    free(g);
}

bool graph_from_file(FILE *file, graph **g)
{
    char buffer[MAXLINELEN];
    char *ptr;
    bool is_defined = false;        /* Keeps track of graph initialization to avoid double allocation. */
    int nvertices = 0;
    int a,b;
   
    *g = NULL; 
    
    while (fgets(buffer, MAXLINELEN, file) != NULL) {
        ptr = strtok(buffer, FILEDELIM);

        switch(*ptr) {
            case 'c':
                /* Comment line. Ignore it. */
                break;

            case 'p':
                /* 
                 *  Instance definition. If more than one p directive present in file, return error.
                 */
                if (is_defined) {
                    print("%s\n", "Graph definitions found at more than one place.");
                    graph_free(*g);
                    return false;
                }

                is_defined = true;

                ptr = strtok(NULL, FILEDELIM);
                strcpy(buffer, ptr);

                if (strcmp(buffer, "edge") == 0) {
                    ptr = strtok(NULL, FILEDELIM);

                    /* 
                     * Read number of vertices. Increment it in one as the format considers graphs starting at node one (not zero).
                     * Left in the line is the number of edges. Ignore it and keep reading edges until end of file. 
                     */
                    strcpy(buffer, ptr);
                    nvertices = strtol(buffer, NULL, 10);
                    /* TODO. In the file the vertex count starts with 1. So either increment here the number of vertices or
                       decrement vertex numbers when inserting edges.*/
                    nvertices++;
                    *g = graph_create(nvertices);
                    //print("Created graph with %d vertices. Reading vertices\n", nvertices);
                } else {
                    print("Graph definition with unknown keyword: %s\n", buffer);
                    return false;
                }
                break;

            case 'e':
                /*
                 * This is an edge definition, like
                 *          e 1 3
                 * for representing 1-3 edge.
                 */
                if (!(*g)) {
                    printf("%s\n", "Found edge definition before graph definition");
                    return false;
                }
/*
                if (nvertices == 0) {
                    printf("%s\n", "Incorrect number of vertices specifed. Ignoring remaining vertices, sorry.");
                    break;
                }

                nvertices--;
  */              
                ptr = strtok(NULL, FILEDELIM);
                strcpy(buffer, ptr);
                a = strtol(buffer, NULL, 10);
                ptr = strtok(NULL, FILEDELIM);
                strcpy(buffer, ptr);
                b = strtol(buffer, NULL, 10);
                /*list_insert((*g)->edges[a-1], vertex_new(b-1));*/
                
                /* It's an undirected graph. */
                (*g)->adj_matrix[a][b] = 1;
                (*g)->adj_matrix[b][a] = 1;
                (*g)->degrees[a]++;
                (*g)->degrees[b]++;
                break;
        }
    }

    (*g)->nvertices = nvertices;

    return true;
}

/*
 * intersection
 *
 * Returns a list of vertices present both in a and b.
 /*
vertex *intersection(vertex *a, vertex *b)
{
    vertex *v;

    for (; a != NULL, b != NULL; ) {
    }

    return v;
}
*/

/*
 * bron_kerbosch
 *
 * Implements the Bron-Kerbosch algorithm for finding maximum cliques on a graph.
 * 
 * Parameters:
 *      r - a list of vertices
 *      p - a list of vertices
 *      x - a list of vertices
 */
/*
vertex *bron_kerbosch(vertex *R, vertex *p, vertex *x)
{
    int i;
    vertex *P = list_copy(p);
    vertex *X = list_copy(x);

    if (() && (x == NULL)) {
        return r;
    }

    for (i = 0; i < p->nvertices; i++) {
        list_insert(R, v);
        bron_kerbosch(r, p[i], p[i]);
        list_remove(P, v);
        list_insert(X, v);
    }

    list_free(P);
    list_free(X);
}
*/

