#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "datatypes.h"
#include "graph.h"
#include "heuristics.h"

bool GLOBAL_VERBOSITY;
graph *g;

void initialize()
{
    srand(time(NULL));
}

void run_tests()
{
    printf("Running test cases.\n");
    printf("Testing f()\n");
    printf("f()...%s.\n", test_f()?"SUCCEEDED":"FAILED");
    printf("Bye.\n");
}

/*
 * show_debug_console
 *
 * Provide debug functions to user. Enable the user to check if a given edge exists, or inspect
 * graph adjacency matrx contents.
 */
void show_debug_console()
{
    bool running = true;
    char buffer[128];
    const char* FILEDELIM = "\t ";
    char *ptr;
    
    printf("Debug mode\n");

    while (running) {
        printf(">");

        scanf("%s", buffer);

        switch (buffer[0]) {

            /*
             * Show edge. Format: e v1 v2
             */
            case 'e':
            case 'E':
                {
                    int v1=0, v2=0;
                    scanf("%d", &v1);
                    scanf("%d", &v2);

                    if ((v1 != 0) && (v2 != 0)) {
                        printf("%s\n", (g->adj_matrix[v1][v2]==0)?"false":"true");
                    } else {
                        printf("Invalid vertices.\n");
                    }
                }
            break;

            /*
             * Show content (peek) of memory. Format: p x y
             */
            case 'p':
            case 'P':
                {
                    int v1=0, v2=0;
                    scanf("%d", &v1);
                    scanf("%d", &v2);

                    printf("%d\n", g->adj_matrix[v1][v2]);
                }
            break;

            /*
             * Quit.
             */
            case 'Q':
            case 'q':
                running = false;
            break;
                
        }
        
    }
}

int main(int argc, char* argv[])
{
    FILE *f;
    int i;
    char *filename;
    enum { PS_NONE, PS_TEMP, PS_STEP } parse_state = PS_NONE;
    double temp = 100, step = 0.996;
    bool debug_mode = false;

    initialize();

    /* Parse command line. */
    if (argc < 2) {
        printf("%s\n", "Error: No input file specified.");
        return 1;
    }

    for(i = 1; i < argc; i++){
        /* Check if param is an option or a filename. */
        if (argv[i][0] == '-'){
            switch (argv[i][1]) {

                case 'D':
                case 'd':
                    debug_mode = true;
                break;
                
                case 'V':
                case 'v': 
                    GLOBAL_VERBOSITY = true;
                break;

                case 'T':
                case 't':
                    parse_state = PS_TEMP;
                break;

                case 'S':
                case 's':
                    parse_state = PS_STEP;
                break;

                case 'U':
                case 'u':
                    run_tests();
                    return 1;

                default:
                    printf("Unknown option %c\n", argv[i][1]);
                    return 1;

            }
        } else {
            switch (parse_state) {
                case PS_NONE:
                    filename = argv[i];
                break;

                case PS_TEMP:
                    temp = atof((argv[i]));

                    if (temp == 0) {
                        printf("Could not parse temperature \"%s\". Exiting.\n", argv[i]);
                        return 1;
                    }

                    parse_state = PS_NONE;
                break;

                case PS_STEP:
                    step = atof(argv[i]);

                    if (step == 0) {
                        printf("Could not parse step. Exiting.\n");
                        return 1;
                    }

                    parse_state = PS_NONE;
                break;
            }
        }
    }

    f = fopen(filename, "r");

    if (f == NULL) {
        printf("%s\n", "Error: Could not open input file.");
        return 1;
    }

    if (!graph_from_file(f, &g)) {
        printf("Could not open file \"%s\"", filename);
        return 1;
    }

    if (debug_mode) {
        show_debug_console();
    } else {
        solution *sol;
        print("---------------------------------------------------------\n");
        print("Beginning Simulated Annealing\n");
        print("Initial temperature: %f\n", temp);
        print("Step: %f\n", step);
        print("---------------------------------------------------------\n");
        sol = gen_init_sol(g);
        simulated_annealing_clique(g, sol, temp, step);
        print("Obtained a solution of size: %zu\n", sol->sol_length);
        solution_dump(sol); 
        solution_free(sol);
        print("Allocated a total of %d MB.\n", get_allocated_bytes()/1024);
    }
    
    graph_free(g);

    fclose(f);

    return 0;
}

