#ifndef _GRAPH_H_
#define _GRAPH_H_
#include <stdbool.h>
#include <stdio.h>

/*
 * graph
 * 
 * A graph representation. Edges are represented as a adjacency list.
 * 
 * To represent an edge i-j we mark
 * 
 */	
/*
typedef struct {
    int n;
    int v;
    struct vertex *next;
} vertex;

typedef struct {
    vertex **edges;
    int *degrees;
    int nvertices;
} graph;
*/

typedef struct {
    char **adj_matrix;
    int *degrees;
    int nvertices;
} graph;

graph *graph_create(int nvertices);
graph *graph_copy(graph *g);
void graph_free(graph *g);
bool graph_from_file(FILE *file, graph **g);
/*vertex* vertex_new(int v);*/

#endif

