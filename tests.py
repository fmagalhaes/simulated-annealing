from collections import defaultdict
import subprocess 
import time

edges = defaultdict(list)

with open('instances/brock800_1.clq') as f:
    lines = f.readlines()

for l in lines:
    e = l.split()
    if e[0] == 'e':
        edges[int(e[1])].append(int(e[2]))

def check_clique(clique):
    for i in range(0, len(clique)):
        for j in range(0, len(clique)):
            if i != j:
                m = max(int(clique[i]), int(clique[j]))
                l = min(int(clique[i]), int(clique[j]))

                #print 'Max: %d Min: %d' %(m, l)

                if l not in edges[m]:
                    print 'Not a clique: %d-%d' % (m,l)
                    return False

    return True


for i in range(0, 5):
    args = ('./clique', '-t', '100', '-s', '0.996', 'instances/brock800_1.clq')
    popen = subprocess.Popen(args, stdout=subprocess.PIPE)
    popen.wait()
    output = popen.stdout.read()
    output = output.split()
    print 'Checking clique: %s of size %d' % (output, len(output))  
    if check_clique(output):
        print 'OK'
    else:
        print 'FAIL'
        break
    print 'Sleeping 2 seconds..\n'
    time.sleep(2)


