#ifndef _HEURISTICS_H_
#define _HEURISTICS_H_

#include "graph.h"

#define EDGE(gr, i1, i2) gr->adj_matrix[i1][i2] = gr->adj_matrix[i2][i1] = 1;
#define ARRAY_SWAP(arr, p1, p2) do { arr[p1] ^= arr[p2]; arr[p2] ^= arr[p1]; arr[p1] ^= arr[p2];  } while(0)

typedef struct {
    void *arr;
    size_t arr_length;
    size_t sol_length;
    struct solution *(*neighbour_function)(struct solution *);
} solution;

enum problem_type {MAXIMIZATION, MINIMIZATION};

solution *solution_create(size_t len);
void solution_free(solution *sol);
void solution_dump(solution *sol);
solution *simulated_annealing_clique(solution *initial_solution, double T, double cooling_fact, problem_type type);

/*
 * Tests
 */
bool test_f(void);


#endif

