#ifndef _TOOLS_H_
#define _TOOLS_H_

#include <stdlib.h>

void print(const char *fmt, ...);
void *emalloc(size_t size);
size_t get_allocated_bytes();
void *strscpy(char dst[], const char *ori, size_t n);
#endif

