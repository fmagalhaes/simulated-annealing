OBJECTS = main.o datatypes.o graph.o tools.o heuristics.o
CFLAGS = -g

all: clique

clique: $(OBJECTS)
	gcc main.o datatypes.o graph.o tools.o heuristics.o -o clique -lm

main.o: main.c
	gcc ${CFLAGS} -c main.c

datatypes.o: datatypes.c
	gcc ${CFLAGS} -c datatypes.c

graph.o: graph.c
	gcc ${CFLAGS} -c graph.c 

tools.o: tools.c
	gcc ${CFLAGS} -c tools.c

heuristics.o: heuristics.c
	gcc ${CFLAGS} -c heuristics.c

clean:
	rm -rf *o clique
