#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tools.h"

/* Controls the verbosity level. Is set on main.c module. */
extern bool GLOBAL_VERBOSITY;

static size_t allocated_bytes;

/*
 * print
 *
 * Prints debug information based on verbosity level set up. 
 */
void print(const char *fmt, ...)
{
    if (GLOBAL_VERBOSITY){
        va_list args;
        va_start(args, fmt);
        vprintf(fmt, args);
        va_end(args);
    }
}

/*
 * emalloc
 *
 * Wrapper around stdlib malloc function. Adds error handling and zero out allocated memory before
 * returning to user.
 */
void *emalloc(size_t size)
{   
    void *v = malloc(size);

    if (v == NULL) {
        printf("Failling allocating %zu bytes.", size);
        abort();
    }
    
    allocated_bytes += size;
    memset(v, 0, size);
    return v;
}  

size_t get_allocated_bytes()
{
    return allocated_bytes;
}

void *strscpy(char dst[], const char *ori, size_t n)
{
    memcpy(dst, ori, n-1);
    dst[n-1] = '\0';
    return dst;
}

/*
 * rand_interval
 *
 * Return an int random number in the interval [a,b].
 */
int rand_interval(int a, int b)
{
    return a + (rand() % b);
}

/*
 * rand_01
 *
 * Return a double random number in the interval [0,1].
 */
double rand_01(void)
{
    return rand()/(double)RAND_MAX;
}

