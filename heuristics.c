/*
 * heuristics.c
 *
 * Implement Simmulated Annealing heuristic for finding cliques in graphs and it's auxiliary functions.
 *
 */
#include <math.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include "datatypes.h"
#include "graph.h"
#include "heuristics.h"
#include "tools.h"

solution *solution_create(size_t arr_len, size_t data_size)
{
    solution *sol = emalloc(sizeof(solution));

    sol->arr = emalloc(data_size * len);
    sol->arr_length = len;
    return sol;
}

void solution_free(solution *sol)
{
    free(sol->arr);
    free(sol);
}

void solution_dump(solution *sol)
{
    size_t i;
    for (i = 0; i < sol->sol_length; i++) {
        printf("%d ", sol->arr[i]);
    }
    printf("\n");
}

static bool prob(float exponent)
{
    double rnd = rand()/(double)RAND_MAX;   
    return rnd < exp(exponent);
}

static int rand_2()
{
    return (rand()/(float)RAND_MAX) < 0.5
            ? 
                0
            :
                1;
}


/*
 * simulated_annealing_clique
 *
 * Execute the simmulated annealing heuristic to find the maximum clique on a graph.
 *
 * Parameters:
 *      - initial_solution - an initial solution for the problem (exact or not)
 *      - T - the initial "temperature". This is directly related to the number of times the heuristics will execute
 *      - cooling_fact - The decrement of the temperature T in each loop of the algorithm. Must be in the interval [0,1]
 *      - f - pointer to function responsible for computing the objective of a given solution
 *      - get_neighbour - Pointer to function that generates neighbour solutions to a given solution
 *
 * Return
 *      A pointer to an array of vertices representing a solution. Don't forget to free allocated memory for solution.
 */
solution *simulated_annealing(solution *initial_solution, double T, double cooling_fact, problem_type type)
{
    int delta;
    int i;
    int j;
    int n=0;
    solution *sol = initial_solution;
    solution sol_;
    int best_f = 0;                         /* Objective of best solution so far */
    int cur_f = 0;                          /* Objective of current solution */
    int exiting, entering;                  /* Exiting and entering vertices */
    bool give_up = false;
    bool p;
    neighbour_function get_neighbour = initial_solution->neighbour;

    for (i = 0; i < 20; i++) {
        for (j = 0; j < 20; j++) {
            sol_ = neighbour(sol);

            if (type == MAXIMIZE)
                delta = sol_.f - sol.f;
            else
                delta = sol.f - sol_.f

            /* Add one to delta to avoid computing probability with a 0 delta, what would yield a very high probability. */
            p = prob((-((double)delta+1))/T);

            /*
             * If new solution is better than current best solution or it is elected with propability -delta/T, 
             * free current best solution and take the new solution as our best. Otherwise, discard new solution.
             */
            if ((delta > 0) || p) {
                solution_free(sol);
                sol = sol_;
            } else {
                solution_free(sol);
            }

            T *= cooling_fact;
        }
    }

    return sol;
}

