#ifndef _DATATYPES_H_
#define _DATATYPES_H_
#include <stdbool.h>
#include "graph.h"

#define MAXSTACKSIZE 200

typedef struct {
    int data[MAXSTACKSIZE];
    int size;
    int top;
} stack;

/*
void list_insert(vertex *l, vertex *node);
void list_free(vertex *l);
bool list_remove(vertex *l, vertex *v);
void list_apply(vertex *l, void (*fn)(vertex*));
*/

/*
 * Definitions taken from "The Practice of Programming" by Kernighan and Pike.
 */
enum {
    NHASH = 4093,               /* Default hash array size. */
    MULTIPLIER = 31
};

typedef struct {
    size_t value;
    struct hash_node *next;	
} hash_node;

size_t hash(size_t v);
hash_node *hash_lookup(hash_node *table[], int value, int create);
#endif

