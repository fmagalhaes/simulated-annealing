#include <assert.h>
#include "datatypes.h"
#include "tools.h"

/*
void list_insert(vertex *l, vertex *node)
{
    vertex *tail;

    for (; l != NULL; l = l->next) {
        tail = l;
    }

    tail->next = node;
}
*/

/*
 * list_apply
 *
 * Apply fn function to each member of vertice list l.
 */
/*
void list_apply(vertex *l, void (*fn)(vertex*))
{
    vertex *ptr = l;

    for (; ptr != NULL; ptr = ptr->next) {
        fn(ptr);
    }
}
*/

/*
 * list_remove
 * 
 * Remove vertex v from list l.
 *
 * Return:
 *      true - if node was present and was excluded
 *      false - otherwise
 */
/*
bool list_remove(vertex *l, vertex *v)
{
    vertex *ptr;
    vertex *prev = NULL;

    for (ptr = l; ptr != NULL ; ptr = ptr->next) {
        if (ptr->v == v->v) {
            if (prev != NULL)    
                prev->next = ptr->next;
            
            free(ptr);
            return true;
        }
        prev = ptr;
    }

    return false;
}
*/
/*
void list_free(vertex *l)
{
    void *next;

    for (; l != NULL; l = next) {
        next = l->next;
        free(l);
    }
}

vertex *vertex_new(int v)
{
    vertex *vert = (vertex *)malloc(sizeof(v));
    vert->v = v;
    vert->next = NULL;
    return vert;
}

void list_copy(vertex *l)
{
    vertex *head;
    vertex *ptr;
    
    assert(l); 
    head = vertex_new(l->v);
    ptr = l->next;
    for (; ptr != NULL; ptr = ptr->next) {
        list_insert(head, ptr);
    }
}
*/

/* Knuth multiplicative hash */
size_t hash(size_t v)
{
    return (v*2654435761) % NHASH;
}

hash_node *hash_lookup(hash_node *table[], int value, int create)
{
    int h;
    hash_node *sym;

    h = hash(value);
    for (sym = table[h]; sym != NULL; sym = sym->next) {
        if (sym->value == value)
            return sym;
    }

    if (create) {
        sym = (hash_node*)emalloc(sizeof(hash_node));
        sym->value = value;
        sym->next = table[h];
        table[h] = sym;
    }

    return sym;
}

